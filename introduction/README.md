## 版本说明

本示例基于华为云SDK V3.0版本开发

## 功能介绍

基于华为云 Java SDK对云办公桌面进行操作的代码示例。
其中包括：对云办公桌面进行操作的增（创建桌面）、删（删除单个桌面）、改（操作桌面）、查（查询桌面列表）。
本示例将以上操作整合，完成了从云办公桌面的创建到查询、修改，再到删除的整个生命周期的覆盖。

## 前置条件

### 开通服务

申请开通云办公服务的具体操作请参考[开通云办公服务API](https://support.huaweicloud.com/api-workspace/ApplyWorkspace.html)

### 获取AK/SK

开发者在使用前需先获取账号的ak、sk、projectId、endpoint。

您需要拥有华为云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK/SK。更多信息请查看访问密钥。
endpoint 华为云各服务应用区域和各服务的终端节点，详情请查看地区和终端节点。
projectId 云服务所在项目 ID ，根据你想操作的项目所属区域选择对应的项目 ID 。

### 运行环境

Java JDK 1.8 及其以上版本，推荐通过Maven 安装依赖的方式使用JAVA版本SDK。

### SDK获取和安装

在Maven 项目的 pom.xml 文件加入相应版本的依赖项即可。

以引入3.1.5版本的内容审核SDK为例：

```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-workspace</artifactId>
    <version>3.1.5</version>
</dependency>

```

## 示例代码

调用前请替换示例中的变量，请根据需要执行的方法注释掉无关方法，并执行Run Application。

```java
package com.huawei.workspace.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.workspace.v2.WorkspaceClient;
import com.huaweicloud.sdk.workspace.v2.model.BatchActionDesktopsReq;
import com.huaweicloud.sdk.workspace.v2.model.BatchRunDesktopsRequest;
import com.huaweicloud.sdk.workspace.v2.model.BatchRunDesktopsResponse;
import com.huaweicloud.sdk.workspace.v2.model.CreateDesktopReq;
import com.huaweicloud.sdk.workspace.v2.model.CreateDesktopRequest;
import com.huaweicloud.sdk.workspace.v2.model.CreateDesktopResponse;
import com.huaweicloud.sdk.workspace.v2.model.DeleteDesktopRequest;
import com.huaweicloud.sdk.workspace.v2.model.DeleteDesktopResponse;
import com.huaweicloud.sdk.workspace.v2.model.Desktop;
import com.huaweicloud.sdk.workspace.v2.model.ListDesktopsRequest;
import com.huaweicloud.sdk.workspace.v2.model.ListDesktopsResponse;
import com.huaweicloud.sdk.workspace.v2.model.Volume;
import com.huaweicloud.sdk.workspace.v2.region.WorkspaceRegion;

import java.util.ArrayList;
import java.util.List;

public class WorkspaceDesktopsManageDemo {

    public static void main(String[] args) {
        // 设置AK和SK
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";
        String projectId = "<YOUR PROJECTID>";

        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        WorkspaceClient client = WorkspaceClient.newBuilder()
            .withCredential(auth)
            .withRegion(WorkspaceRegion.valueOf("cn-north-4")) // 示例中区域为北京四，根据实际调用区域替换
            .withHttpConfig(config)
            .build();
        // 查询桌面列表
        listDesktops(client);
        // 创建桌面
        // createDesktop(client);
        // 操作桌面
        // batchRunDesktops(client);
        // 删除单个桌面
        // deleteDesktop(client);
    }

    private static void listDesktops(WorkspaceClient workspaceClient) {
        try {
            ListDesktopsResponse listDesktopsResponse = workspaceClient.listDesktops(new ListDesktopsRequest());
            System.out.println(listDesktopsResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void createDesktop(WorkspaceClient workspaceClient) {
        String userName = "{your userName string}";
        String userEmail = "{your userEmail string}";
        String userGroup = "{your userGroup string}";
        // 套餐ID
        String productId = "{your productId string}";
        // 镜像ID
        String imageId = "{your imageId string}";
        // 镜像类型。private：私有镜像。gold：公共镜像。
        String imageType = "{your imageType string}";
        // 系统盘类型。SAS：高IO。 SSD：超高IO。
        String volumeType = "{your volumeType string}";
        // 磁盘容量，单位GB。系统盘大小范围[80-32760]，数据盘范围[10-32760]，大小为10的倍数。
        Integer volumeSize = 80;
        try {
            List<Desktop> desktops = new ArrayList<>();
            desktops.add(new Desktop().withUserName(userName).withUserEmail(userEmail).withUserGroup(userGroup));
            CreateDesktopReq createDesktopReq = new CreateDesktopReq();
            createDesktopReq.withProductId(productId)
                .withImageId(imageId)
                .withImageType(imageType)
                .withDesktopType(CreateDesktopReq.DesktopTypeEnum.DEDICATED)
                .withDesktops(desktops)
                .withRootVolume(new Volume().withType(volumeType).withSize(volumeSize));
            CreateDesktopResponse createDesktopResponse =
                workspaceClient.createDesktop(new CreateDesktopRequest().withBody(createDesktopReq));
            System.out.println(createDesktopResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void batchRunDesktops(WorkspaceClient workspaceClient) {
        String desktopId = "{your desktopId string}";
        // 操作类型。 os-start 启动。 reboot 重启。 os-stop 关机。
        String opType = "{your opType string}";
        try {
            List<String> desktopIds = new ArrayList<>();
            desktopIds.add(desktopId);

            BatchActionDesktopsReq batchActionDesktopsReq = new BatchActionDesktopsReq();
            batchActionDesktopsReq.withDesktopIds(desktopIds).withOpType(opType);
            BatchRunDesktopsResponse batchRunDesktopsResponse =
                workspaceClient.batchRunDesktops(new BatchRunDesktopsRequest().withBody(batchActionDesktopsReq));
            System.out.println(batchRunDesktopsResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void deleteDesktop(WorkspaceClient workspaceClient) {
        String desktopId = "{your desktopId string}";
        try {
            DeleteDesktopResponse deleteDesktopResponse =
                workspaceClient.deleteDesktop(new DeleteDesktopRequest().withDesktopId(desktopId));
            System.out.println(deleteDesktopResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
```

## 参考

更多信息请参考：
[查询桌面列表API](https://support.huaweicloud.com/api-workspace/ListDesktops.html)
[创建桌面API](https://support.huaweicloud.com/api-workspace/CreateDesktop.html)
[删除单个桌面API](https://support.huaweicloud.com/api-workspace/DeleteDesktop.html)
[操作桌面API](https://support.huaweicloud.com/api-workspace/BatchRunDesktops.html)

## 修订记录

|  发布日期  | 文档版本 |   修订说明   |
| :--------: | :------: | :----------: |
| 2022-10-17 |   1.0    | 文档首次发布 |