
package com.huawei.workspace.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.core.http.HttpConfig;
import com.huaweicloud.sdk.workspace.v2.WorkspaceClient;
import com.huaweicloud.sdk.workspace.v2.model.BatchActionDesktopsReq;
import com.huaweicloud.sdk.workspace.v2.model.BatchRunDesktopsRequest;
import com.huaweicloud.sdk.workspace.v2.model.BatchRunDesktopsResponse;
import com.huaweicloud.sdk.workspace.v2.model.CreateDesktopReq;
import com.huaweicloud.sdk.workspace.v2.model.CreateDesktopRequest;
import com.huaweicloud.sdk.workspace.v2.model.CreateDesktopResponse;
import com.huaweicloud.sdk.workspace.v2.model.DeleteDesktopRequest;
import com.huaweicloud.sdk.workspace.v2.model.DeleteDesktopResponse;
import com.huaweicloud.sdk.workspace.v2.model.Desktop;
import com.huaweicloud.sdk.workspace.v2.model.ListDesktopsRequest;
import com.huaweicloud.sdk.workspace.v2.model.ListDesktopsResponse;
import com.huaweicloud.sdk.workspace.v2.model.Volume;
import com.huaweicloud.sdk.workspace.v2.region.WorkspaceRegion;

import java.util.ArrayList;
import java.util.List;

public class WorkspaceDesktopsManageDemo {

    public static void main(String[] args) {
        // 设置AK和SK
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";
        String projectId = "<YOUR PROJECTID>";

        ICredential auth = new BasicCredentials().withAk(ak).withSk(sk).withProjectId(projectId);

        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        WorkspaceClient client = WorkspaceClient.newBuilder()
            .withCredential(auth)
            .withRegion(WorkspaceRegion.valueOf("cn-north-4")) // 示例中区域为北京四，根据实际调用区域替换
            .withHttpConfig(config)
            .build();
        // 查询桌面列表
        listDesktops(client);
        // 创建桌面
        // createDesktop(client);
        // 操作桌面
        // batchRunDesktops(client);
        // 删除单个桌面
        // deleteDesktop(client);
    }

    private static void listDesktops(WorkspaceClient workspaceClient) {
        try {
            ListDesktopsResponse listDesktopsResponse = workspaceClient.listDesktops(new ListDesktopsRequest());
            System.out.println(listDesktopsResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void createDesktop(WorkspaceClient workspaceClient) {
        String userName = "{your userName string}";
        String userEmail = "{your userEmail string}";
        String userGroup = "{your userGroup string}";
        // 套餐ID
        String productId = "{your productId string}";
        // 镜像ID
        String imageId = "{your imageId string}";
        // 镜像类型。private：私有镜像。gold：公共镜像。
        String imageType = "{your imageType string}";
        // 系统盘类型。SAS：高IO。 SSD：超高IO。
        String volumeType = "{your volumeType string}";
        // 磁盘容量，单位GB。系统盘大小范围[80-32760]，数据盘范围[10-32760]，大小为10的倍数。
        Integer volumeSize = 80;
        try {
            List<Desktop> desktops = new ArrayList<>();
            desktops.add(new Desktop().withUserName(userName).withUserEmail(userEmail).withUserGroup(userGroup));
            CreateDesktopReq createDesktopReq = new CreateDesktopReq();
            createDesktopReq.withProductId(productId)
                .withImageId(imageId)
                .withImageType(imageType)
                .withDesktopType(CreateDesktopReq.DesktopTypeEnum.DEDICATED)
                .withDesktops(desktops)
                .withRootVolume(new Volume().withType(volumeType).withSize(volumeSize));
            CreateDesktopResponse createDesktopResponse =
                workspaceClient.createDesktop(new CreateDesktopRequest().withBody(createDesktopReq));
            System.out.println(createDesktopResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void batchRunDesktops(WorkspaceClient workspaceClient) {
        String desktopId = "{your desktopId string}";
        // 操作类型。 os-start 启动。 reboot 重启。 os-stop 关机。
        String opType = "{your opType string}";
        try {
            List<String> desktopIds = new ArrayList<>();
            desktopIds.add(desktopId);

            BatchActionDesktopsReq batchActionDesktopsReq = new BatchActionDesktopsReq();
            batchActionDesktopsReq.withDesktopIds(desktopIds).withOpType(opType);
            BatchRunDesktopsResponse batchRunDesktopsResponse =
                workspaceClient.batchRunDesktops(new BatchRunDesktopsRequest().withBody(batchActionDesktopsReq));
            System.out.println(batchRunDesktopsResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }

    private static void deleteDesktop(WorkspaceClient workspaceClient) {
        String desktopId = "{your desktopId string}";
        try {
            DeleteDesktopResponse deleteDesktopResponse =
                workspaceClient.deleteDesktop(new DeleteDesktopRequest().withDesktopId(desktopId));
            System.out.println(deleteDesktopResponse.toString());
        } catch (ConnectionException | RequestTimeoutException e) {
            System.out.println(e.getMessage());
        } catch (ServiceResponseException e) {
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
